package dao;
import modelos.Ronda;
import java.util.List;

import javax.persistence.EntityManager;

public class RondaDAO implements GenericDAO<Ronda>{
	
	private EntityManager manager=DaoUtils.getEntityManager();
	
	@Override
	public List<Ronda> getList() {
		List<Ronda> lista=(List<Ronda>)manager.createQuery("from Ronda").getResultList();
		return lista;
	}

	@Override
	public void agregar(Ronda element) {
		try {
			manager.getTransaction().begin();
			manager.persist(element);
			manager.getTransaction().commit();
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@Override
	public void modificar(Ronda element) {
		element=manager.find(Ronda.class, element.getId());
		try {
			manager.getTransaction().begin();
		    manager.merge(element);
		    manager.getTransaction().commit();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void eliminar(Ronda element) {
		element=manager.find(Ronda.class, element.getId());
		try {
			manager.getTransaction().begin();
		    manager.remove(element);
		    manager.getTransaction().commit();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}
