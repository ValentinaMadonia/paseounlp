package dao;
import java.util.List;

import javax.persistence.EntityManager;

import modelos.Producto;
import utils.Estado;

public class ProductoDAO implements GenericDAO<Producto>{
	
	private EntityManager manager=DaoUtils.getEntityManager();
	
	@Override
	public List<Producto> getList() {
		List<Producto> lista=(List<Producto>)manager.createQuery("from Producto").getResultList();
		return lista;
	}

	@Override
	public void agregar(Producto element) { //agrego primero un productor ,en la prueba hago un element.productor= productor.id?
		try {
			manager.getTransaction().begin();
			manager.persist(element);
			manager.getTransaction().commit();
		} catch (Exception e) {
			manager.getTransaction().rollback();
		}
	}

	@Override
	public void modificar(Producto element) {
		element=manager.find(Producto.class, element.getId());
		try {
			manager.getTransaction().begin();
		    manager.merge(element);
		    manager.getTransaction().commit();
		}catch(Exception e) {
			manager.getTransaction().rollback();
		}
	}

	@Override
	public void eliminar(Producto element) {
		element=manager.find(Producto.class, element.getId());
		element.setEstado(Estado.SUSPENDIDO);
		try {
			manager.getTransaction().begin();
		    manager.merge(element);
		    manager.getTransaction().commit();
		}catch(Exception e) {
			manager.getTransaction().rollback();
		}
	}
	public void activar(Producto element) {
		element=manager.find(Producto.class, element.getId());
		element.setEstado(Estado.ACTIVO);
		try {
			manager.getTransaction().begin();
		    manager.merge(element);
		    manager.getTransaction().commit();
		}catch(Exception e) {
			manager.getTransaction().rollback();
		}
	}

}
