package dao;
import modelos.Productor;
import utils.Estado;

import java.util.List;

import javax.persistence.EntityManager;

public class ProductorDAO implements GenericDAO<Productor> {

	private EntityManager manager=DaoUtils.getEntityManager();
	
	@Override
	public List<Productor> getList() {
		List<Productor> lista=(List<Productor>)manager.createQuery("from Productor").getResultList();
		return lista;
	}

	@Override
	public void agregar(Productor element) {
		try {
			manager.getTransaction().begin();
			manager.persist(element);
			manager.getTransaction().commit();
		} catch (Exception e) {
			//manager.getTransaction().rollback();
		}
	}

	@Override
	public void modificar(Productor element) {
		element=manager.find(Productor.class, element.getId());
		try {
			manager.getTransaction().begin();
		    manager.merge(element);
		    manager.getTransaction().commit();
		}catch(Exception e) {
			manager.getTransaction().rollback();
		}
	}

	@Override
	public void eliminar(Productor element) {
		element=manager.find(Productor.class, element.getId());
		element.setEstado(Estado.SUSPENDIDO);
		try {
			manager.getTransaction().begin();
		    manager.merge(element);
		    manager.getTransaction().commit();
		}catch(Exception e) {
			manager.getTransaction().rollback();
		}
	}
	
	public void activar(Productor element) {
		element=manager.find(Productor.class, element.getId());
		element.setEstado(Estado.ACTIVO);
		try {
			manager.getTransaction().begin();
		    manager.merge(element);
		    manager.getTransaction().commit();
		}catch(Exception e) {
			manager.getTransaction().rollback();
		}
	}
}
