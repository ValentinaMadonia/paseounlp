package dao;

import java.util.List;

import javax.persistence.EntityManager;
import modelos.PuntoDeRetiro;

public class PuntoDeRetiroDAO implements GenericDAO<PuntoDeRetiro>{

	private EntityManager manager=DaoUtils.getEntityManager();
	
	@Override
	public List<PuntoDeRetiro> getList() {
		List<PuntoDeRetiro> lista=(List<PuntoDeRetiro>)manager.createQuery("from PuntoDeRetiro").getResultList();
		return lista;
	}

	@Override
	public void agregar(PuntoDeRetiro element) {
		try {
			manager.getTransaction().begin();
			manager.persist(element);
			manager.getTransaction().commit();
		} catch (Exception e) {
			e.getStackTrace();
		}
		
	}

	@Override
	public void modificar(PuntoDeRetiro element) { //recibir elemento modificado
		element=manager.find(PuntoDeRetiro.class, element.getId());
		try {
			manager.getTransaction().begin();
		    manager.merge(element);
		    manager.getTransaction().commit();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void eliminar(PuntoDeRetiro element) {
		element=manager.find(PuntoDeRetiro.class, element.getId());
		try {
			manager.getTransaction().begin();
			manager.remove(element);
			manager.getTransaction().commit();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	

}
