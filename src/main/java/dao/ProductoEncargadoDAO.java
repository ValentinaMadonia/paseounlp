package dao;
import java.util.List;

import javax.persistence.EntityManager;

import modelos.ProductoEncargado;
import utils.Estado;   
public class ProductoEncargadoDAO implements GenericDAO<ProductoEncargado> {

	private EntityManager manager=DaoUtils.getEntityManager();
	
	@Override
	public List<ProductoEncargado> getList() {
		List<ProductoEncargado> lista=(List<ProductoEncargado>)manager.createQuery("from ProductoEncargado").getResultList();
		return lista;
	}

	@Override
	public void agregar(ProductoEncargado element) {
		try {
			manager.getTransaction().begin();
			manager.persist(element);
			manager.getTransaction().commit();
		} catch (Exception e) {
			manager.getTransaction().rollback();
		}
	}

	@Override
	public void modificar(ProductoEncargado element) {
		element=manager.find(ProductoEncargado.class, element.getId());
		try {
			manager.getTransaction().begin();
		    manager.merge(element);
		    manager.getTransaction().commit();
		}catch(Exception e) {
			manager.getTransaction().rollback();
		}
	}

	@Override
	public void eliminar(ProductoEncargado element) { //si se cancela el pedido
		element=manager.find(ProductoEncargado.class, element.getId());
		element.setEstado(Estado.SUSPENDIDO);
		try {
			manager.getTransaction().begin();
			manager.merge(element);
			manager.getTransaction().commit();
		}catch(Exception e) {
			manager.getTransaction().rollback();
		}
	}
	public void activar(ProductoEncargado element) {
		element=manager.find(ProductoEncargado.class, element.getId());
		element.setEstado(Estado.ACTIVO);
		try {
			manager.getTransaction().begin();
			manager.merge(element);
			manager.getTransaction().commit();
		}catch(Exception e) {
			manager.getTransaction().rollback();
		}
	}

}
