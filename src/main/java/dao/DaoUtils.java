package dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DaoUtils {
	private static EntityManagerFactory emf=Persistence.createEntityManagerFactory("persistencia");
	private static EntityManager manager=emf.createEntityManager();
	
	public static EntityManager getEntityManager() {
		return manager;
	}
	
	
}
