package dao;
import java.util.List;

import javax.persistence.EntityManager;

import modelos.RubroDeProducto;
import utils.Estado;
public class RubroDelProductoDAO implements GenericDAO<RubroDeProducto> {

	private EntityManager manager=DaoUtils.getEntityManager();
	
	@Override
	public List<RubroDeProducto> getList() {
		List<RubroDeProducto> lista=(List<RubroDeProducto>)manager.createQuery("from RubroDeProducto").getResultList();
		return lista;
	}

	@Override
	public void agregar(RubroDeProducto element) {
		try {
			manager.getTransaction().begin();
			manager.persist(element);
			manager.getTransaction().commit();
		} catch (Exception e) {
			manager.getTransaction().rollback();
		}
	}

	@Override
	public void modificar(RubroDeProducto element) {
		element=manager.find(RubroDeProducto.class, element.getId());
		try {
			manager.getTransaction().begin();
		    manager.merge(element);
		    manager.getTransaction().commit();
		}catch(Exception e) {
			manager.getTransaction().rollback();
		}
	}

	@Override
	public void eliminar(RubroDeProducto element) {
		element=manager.find(RubroDeProducto.class, element.getId());
		element.setEstado(Estado.SUSPENDIDO);
		try {
			manager.getTransaction().begin();
		    manager.merge(element);
		    manager.getTransaction().commit();
		}catch(Exception e) {
			manager.getTransaction().rollback();
		}
	}
	
	public void activar(RubroDeProducto element) {
		element=manager.find(RubroDeProducto.class, element.getId());
		element.setEstado(Estado.ACTIVO);
		try {
			manager.getTransaction().begin();
		    manager.merge(element);
		    manager.getTransaction().commit();
		}catch(Exception e) {
			manager.getTransaction().rollback();
		}
	}

}
