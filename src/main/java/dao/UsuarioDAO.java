package dao;

import java.util.List;

import javax.persistence.EntityManager;

import modelos.Usuario;
import utils.Estado;
import utils.Rol;

public class UsuarioDAO implements GenericDAO<Usuario>{

	private EntityManager manager=DaoUtils.getEntityManager();
	
	@Override
	public List<Usuario> getList() {
		List<Usuario> lista=(List<Usuario>)manager.createQuery("from Usuario").getResultList();
		return lista;
	}

	@Override
	public void agregar(Usuario element) {
		try {
			manager.getTransaction().begin();
			manager.persist(element);
			manager.getTransaction().commit();
		} catch (Exception e) {
			manager.getTransaction().rollback();
		}
	}

	@Override
	public void modificar(Usuario element) {
		element=manager.find(Usuario.class, element.getId());
		try {
			manager.getTransaction().begin();
		    manager.merge(element);
		    manager.getTransaction().commit();
		}catch(Exception e) {
			manager.getTransaction().rollback();
		}
	}

	@Override
	public void eliminar(Usuario element) {
		element=manager.find(Usuario.class, element.getId());
		element.setEstado(Estado.SUSPENDIDO);
		try {
			manager.getTransaction().begin();
			manager.merge(element);
			manager.getTransaction().commit();
		}catch(Exception e) {
			manager.getTransaction().rollback();
		}
	
	}
	public void activar(Usuario element) {
		element=manager.find(Usuario.class, element.getId());
		element.setEstado(Estado.ACTIVO);
		try {
			manager.getTransaction().begin();
		    manager.merge(element);
		    manager.getTransaction().commit();
		}catch(Exception e) {
			manager.getTransaction().rollback();
		}
	}
	public void adminRol(Usuario element) {
		element=manager.find(Usuario.class, element.getId());
		element.setRol(Rol.ADMINISTRADOR);
		try {
			manager.getTransaction().begin();
		    manager.merge(element);
		    manager.getTransaction().commit();
		}catch(Exception e) {
			manager.getTransaction().rollback();
		}
	}
    public void userRol(Usuario element) {
    	element=manager.find(Usuario.class, element.getId());
		element.setRol(Rol.USUARIO);
		try {
			manager.getTransaction().begin();
		    manager.merge(element);
		    manager.getTransaction().commit();
		}catch(Exception e) {
			manager.getTransaction().rollback();
		}
	}

}
