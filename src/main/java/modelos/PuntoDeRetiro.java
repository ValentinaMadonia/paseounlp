package modelos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class PuntoDeRetiro {

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY) 
	private Long id;
	private String direccion;
	private String nombre;
	
	
	public PuntoDeRetiro() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PuntoDeRetiro(String direccion, String nombre) {
		super();
		this.direccion = direccion;
		this.nombre = nombre;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	@Override
	public String toString() {
		return "PuntoDeRetiro [id=" + id + ", direccion=" + direccion + ", nombre=" + nombre + "]";
	}
}
