package dao;
import java.util.List;

import javax.persistence.EntityManager;

import modelos.Pedido;
import utils.EstadoPedido;
import utils.RetiroEntrega;

public class PedidoDAO implements GenericDAO<Pedido> {

	private EntityManager manager=DaoUtils.getEntityManager();
	
	@Override
	public List<Pedido> getList() {
		List<Pedido> lista=(List<Pedido>)manager.createQuery("from Pedido").getResultList();
		return lista;
	}

	@Override
	public void agregar(Pedido element) {
		try {
			manager.getTransaction().begin();
			manager.persist(element);
			manager.getTransaction().commit();
		} catch (Exception e) {
			manager.getTransaction().rollback();
		}
	}

	@Override
	public void modificar(Pedido element) {
		element=manager.find(Pedido.class, element.getId());
		try {
			manager.getTransaction().begin();
		    manager.merge(element);
		    manager.getTransaction().commit();
		}catch(Exception e) {
			manager.getTransaction().rollback();
		}
	}

	@Override
	public void eliminar(Pedido element) {
		element=manager.find(Pedido.class, element.getId());
		element.setEstado(EstadoPedido.CANCELADO);
		try {
			manager.getTransaction().begin();
			manager.merge(element);
			manager.getTransaction().commit();
		}catch(Exception e) {
			manager.getTransaction().rollback();
		}
	}
	public void activar(Pedido element) { //que vuelva a ser pedido 
		element=manager.find(Pedido.class, element.getId());
		element.setEstado(EstadoPedido.NOCONFIRMADO);
		try {
			manager.getTransaction().begin();
			manager.merge(element);
			manager.getTransaction().commit();
		}catch(Exception e) {
			manager.getTransaction().rollback();
		}
	}
	public void confirmar(Pedido element) { //que confirme el pedido el admin
		element=manager.find(Pedido.class, element.getId());
		element.setEstado(EstadoPedido.CONFIRMADO);
		try {
			manager.getTransaction().begin();
			manager.merge(element);
			manager.getTransaction().commit();
		}catch(Exception e) {
			manager.getTransaction().rollback();
		}
	}
	public void marcarComoRetirado(Pedido element) { 
		element=manager.find(Pedido.class, element.getId());
		element.setEstado(EstadoPedido.RETIRADO);
		try {
			manager.getTransaction().begin();
			manager.merge(element);
			manager.getTransaction().commit();
		}catch(Exception e) {
			manager.getTransaction().rollback();
		}
	}
	public void marcarComoEntregado(Pedido element) { 
		element=manager.find(Pedido.class, element.getId());
		element.setEstado(EstadoPedido.ENTREGADO);
		try {
			manager.getTransaction().begin();
			manager.merge(element);
			manager.getTransaction().commit();
		}catch(Exception e) {
			manager.getTransaction().rollback();
		}
	}
	public void marcarParaEntrega(Pedido element) { 
		element=manager.find(Pedido.class, element.getId());
		element.setRetiroEntrega(RetiroEntrega.ENTREGA);
		try {
			manager.getTransaction().begin();
			manager.merge(element);
			manager.getTransaction().commit();
		}catch(Exception e) {
			manager.getTransaction().rollback();
		}
	}
	public void marcarParaRetiro(Pedido element) { 
		element=manager.find(Pedido.class, element.getId());
		element.setRetiroEntrega(RetiroEntrega.RETIRO);
		try {
			manager.getTransaction().begin();
			manager.merge(element);
			manager.getTransaction().commit();
		}catch(Exception e) {
			manager.getTransaction().rollback();
		}
	}

}
