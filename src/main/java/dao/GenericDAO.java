package dao;

import java.util.List;

public interface GenericDAO <T>{

     List<T> getList();
	 void agregar(T element);
	 void modificar(T element);
	 void eliminar(T element);
	
}
