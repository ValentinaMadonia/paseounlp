package dao;
import java.util.List;
import javax.persistence.EntityManager;
import modelos.Image;

public class ImageDAO  implements GenericDAO<Image>{

	private EntityManager manager=DaoUtils.getEntityManager();
	
	@Override
	public List<Image> getList() {
		List<Image> lista=(List<Image>)manager.createQuery("from Image").getResultList();
		return lista;
	}

	@Override
	public void agregar(Image element) {
		try {
			manager.getTransaction().begin();
			manager.persist(element);
			manager.getTransaction().commit();
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@Override
	public void modificar(Image element) {
		element=manager.find(Image.class, element.getId());
		try {
			manager.getTransaction().begin();
		    manager.merge(element);
		    manager.getTransaction().commit();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void eliminar(Image element) {
		element=manager.find(Image.class, element.getId());
		try {
			manager.getTransaction().begin();
			manager.remove(element);
			manager.getTransaction().commit();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}
