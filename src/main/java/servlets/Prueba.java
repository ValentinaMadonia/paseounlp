package servlets;
 //PRUEBA
import java.io.IOException;
import java.util.List;

import dao.*;
import dao.ProductorDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import modelos.*;
import utils.Estado;
import utils.EstadoPedido;
import utils.RetiroEntrega;
import utils.Rol;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
/**
 * Servlet implementation class Prueba
 */
@WebServlet("/Prueba")
public class Prueba extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private PuntoDeRetiro a,a2,a3;
	private PuntoDeRetiroDAO p=new PuntoDeRetiroDAO();
	private ImageDAO m=new ImageDAO();
	private Image m1,m2,m3,m4,m5,m6,m7,m8,m9,m10;
	private RondaDAO r=new RondaDAO();
	private Ronda r1,r2,r3;
	private ProductorDAO productor=new ProductorDAO();
	private Productor p1,p2,p3;
	private RubroDelProductoDAO rubro=new RubroDelProductoDAO();
	private RubroDeProducto rubro1,rubro2,rubro3,rubro4,rubro5;
	private ProductoDAO producto=new ProductoDAO();
	private Producto producto1,producto2,producto3,producto4;
	private UsuarioDAO user=new UsuarioDAO();
	private Usuario u1,u2,u3;
	private PedidoDAO pedido=new PedidoDAO();
	private Pedido pedido1,pedido2;
	private ProductoEncargadoDAO pe=new ProductoEncargadoDAO();
	private ProductoEncargado pe1,pe2,pe3;
    /**
     * Default constructor. 
     */
    public Prueba() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("PUNTO DE RETIRO");
		System.out.println("-----------------------------------------------------------");
		System.out.println("Agregar puntos de retiros");
		a=new PuntoDeRetiro("Calle 50 &, Av. 120","Facultad de informatica");
		agregarPuntoDeRetiro(a);
		a2=new PuntoDeRetiro("Calle 7 1023","Una direccion");
		agregarPuntoDeRetiro(a2);
		a3=new PuntoDeRetiro("Calle 40 678","Otra direccion");
		agregarPuntoDeRetiro(a3);
		listarPuntoDeRetiro();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Editar puntos de retiros(punto2)");
		a2.setNombre("Cambiado el nombre");
		editarPuntoDeRetiro(a2);
		System.out.println("-----------------------------------------------------------");
		System.out.println("Eliminar punto de retiro(punto1)");
		eliminarPuntoDeRetiro(a);
		System.out.println("-----------------------------------------------------------");
		System.out.println("-----------------------------------------------------------");
		System.out.println("\n");
		
		
		System.out.println("IMAGEN");
		System.out.println("-----------------------------------------------------------");
		System.out.println("Agregar imagenes");
		m1=new Image("bolson","http://imagendelbolson");
		m.agregar(m1);
		m2=new Image("maple huevos","http://imagenmapple");
		m.agregar(m2);
		m3=new Image("papa","http://imagendelapapa");
		m.agregar(m3);
		m4=new Image("productor1","http://imagenproductor1");
		m.agregar(m4);
		m5=new Image("productor2","http://imagenproductor2");
		m.agregar(m5);
		m6=new Image("productor3","http://imagenproductor3");
		m.agregar(m6);
		m7=new Image("producto1","http://imagenproducto1");
		m.agregar(m7);
		m8=new Image("producto2","http://imagenproducto2");
		m.agregar(m8);
		m9=new Image("producto3","http://imagenproducto3");
		m.agregar(m9);
		m10=new Image("producto4","http://imagenproducto4");
		m.agregar(m10);
		listarImagenes();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Editar imagen(imagen1)");
		m1.setImagen("http://imagenMODIFICADA");
		m.modificar(m1);
		listarImagenes();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Eliminar imagen(imagen1)");
		m.eliminar(m1);
		listarImagenes();
		System.out.println("-----------------------------------------------------------");
		System.out.println("-----------------------------------------------------------");
		System.out.println("\n");
		
		
		System.out.println("RONDA");
		System.out.println("-----------------------------------------------------------");
		System.out.println("Agregar rondas");
		r1=new Ronda(new Date(2023,6,5),new Date(2023,6,9),new Date(2023,6,10),"9am-15pm");
		r2=new Ronda(new Date(2023,6,12),new Date(2023,6,16),new Date(2023,6,17),"9am-17pm");
		r3=new Ronda(new Date(2023,6,19),new Date(2023,6,23),new Date(2023,6,24),"9am-17pm");
		r.agregar(r1);
		r.agregar(r2);
		r.agregar(r3);
		listarRondas();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Editar ronda(rango horario del 1)");
		r1.setRangoHorario("9am-17pm");
		r.modificar(r1);
		listarRondas();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Eliminar ronda(ronda3)");
		r.eliminar(r3);
		listarRondas();
		System.out.println("-----------------------------------------------------------");
		System.out.println("-----------------------------------------------------------");
		System.out.println("\n");
		
		
		System.out.println("PRODUCTOR");
		System.out.println("-----------------------------------------------------------");
		System.out.println("Agregar productor");
		List<Image>imagenes=new ArrayList<Image>();
		imagenes.add(m3);
		imagenes.add(m2);
		List<Producto>productos=new ArrayList<Producto>();
		p1=new Productor("Pedro","venta de verduras",Estado.ACTIVO,new ArrayList<Producto>(),imagenes);
		productor.agregar(p1);
		List<Image>imagenes2=new ArrayList<Image>();
		imagenes2.add(m4);
		List<Producto>productos2=new ArrayList<Producto>();
		p2=new Productor("Vivivana","venta de frutas y verduras",Estado.ACTIVO,new ArrayList<Producto>(),imagenes2);
		productor.agregar(p2);
		List<Image>imagenes3=new ArrayList<Image>();
		imagenes3.add(m5);
		List<Producto>productos3=new ArrayList<Producto>();
		p3=new Productor("Susana","venta de comidas",Estado.ACTIVO,new ArrayList<Producto>(),imagenes3);
		productor.agregar(p3);
		listarProductores();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Editar productor(productor 3 se agrego imagen)");
		imagenes3=p3.getImagenes();
		Image mnueva=new Image("pizza","http://imagendepizza");
		m.agregar(mnueva);
		imagenes3.add(mnueva);
		p3.setImagenes(imagenes3);
		productor.modificar(p3);
		listarProductores();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Eliminar productor(productor3)");
		productor.eliminar(p3);
		listarProductores();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Activar productor(productor3)");
		productor.activar(p3);
		listarProductores(); 
		System.out.println("-----------------------------------------------------------");
		System.out.println("-----------------------------------------------------------");
		System.out.println("\n");
		
		
		
		System.out.println("RUBRO");
		System.out.println("-----------------------------------------------------------");
		System.out.println("Agregar rubro");
		List<Producto> rproductos1=new ArrayList<Producto>();
		rubro1=new RubroDeProducto("Verdura",Estado.ACTIVO,new ArrayList<Producto>());
		List<Producto> rproductos2=new ArrayList<Producto>();
		rubro2=new RubroDeProducto("Fruta",Estado.ACTIVO,new ArrayList<Producto>());
		List<Producto> rproductos3=new ArrayList<Producto>();
		rubro3=new RubroDeProducto("Carne",Estado.ACTIVO,new ArrayList<Producto>());
		List<Producto> rproductos4=new ArrayList<Producto>();
		rubro4=new RubroDeProducto("Congelado",Estado.ACTIVO,new ArrayList<Producto>());
		List<Producto> rproductos5=new ArrayList<Producto>();
		rubro5=new RubroDeProducto("Lacteo",Estado.ACTIVO,rproductos5);
		rubro.agregar(rubro1);
		rubro.agregar(rubro2);
		rubro.agregar(rubro3);
		rubro.agregar(rubro4);
		rubro.agregar(rubro5);
		listarRubros();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Editar rubro(rubro5)");
		rubro5.setNombre("Artesania");
		rubro.modificar(rubro5);
		listarRubros();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Eliminar rubro(rubro5)");
		rubro.eliminar(rubro5);
		listarRubros();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Activar rubro(rubro5)");
		rubro.activar(rubro5);
		listarRubros();
		System.out.println("-----------------------------------------------------------");
		System.out.println("-----------------------------------------------------------");
		System.out.println("\n");
		
		
		System.out.println("PRODUCTO");
		System.out.println("-----------------------------------------------------------");
		System.out.println("Agregar productos");
		
		List<RubroDeProducto> listar=new ArrayList<RubroDeProducto>();
		listar.add(rubro1);
		List<Image> i=new ArrayList<Image>();
		i.add(m7);
		producto1=new Producto("Lechuga",46,"Lechuga fresca", new BigDecimal(160),Estado.ACTIVO,p2,i,listar);
		producto.agregar(producto1);
		
		List<RubroDeProducto> listar2=new ArrayList<RubroDeProducto>();
		listar2.add(rubro2);
		List<Image> i2=new ArrayList<Image>();
		i2.add(m8);
		producto2=new Producto("Frutilla",50,"Frutillas",new BigDecimal(325),Estado.ACTIVO,p2,i2,listar2);
		producto.agregar(producto2);
		
		List<RubroDeProducto> listar3=new ArrayList<RubroDeProducto>();
		listar3.add(rubro5);
		List<Image> i3=new ArrayList<Image>();
		i3.add(m9);
		producto3=new Producto("Cinto",35,"Cintos negros y marrones",new BigDecimal(430),Estado.ACTIVO,p1,i3,listar3);
		producto.agregar(producto3);

		List<RubroDeProducto> listar4=new ArrayList<RubroDeProducto>();
		listar4.add(rubro3);
		listar4.add(rubro4);
		List<Image> i4=new ArrayList<Image>();
		i4.add(m10);
		producto4=new Producto("Milanesa de carne",60,"Milanesas de bola de lomo",new BigDecimal(1500),Estado.ACTIVO,p3,i4,listar4);
		producto.agregar(producto4);
		
		//agrego los productos al productor
		productos2.add(producto1);
		productos2.add(producto2);
		p2.setProductos(productos2);
		productor.modificar(p2);
		productos.add(producto3);
		p1.setProductos(productos);
		productor.modificar(p1);
		productos3.add(producto4);
		p3.setProductos(productos3);
		productor.modificar(p3);
		//agrego el producto al rubro
		rproductos1.add(producto1);
		rubro1.setProductos(rproductos1);
		rubro.modificar(rubro1);
		rproductos2.add(producto2);
		rubro2.setProductos(rproductos2);
	    rubro.modificar(rubro2);
	    rproductos5.add(producto3);
		rubro5.setProductos(rproductos5);
		rubro.modificar(rubro5);
		rproductos3.add(producto4);
		rubro3.setProductos(rproductos3);
		rproductos4.add(producto4);
		rubro4.setProductos(rproductos4);
		rubro.modificar(rubro3);
		rubro.modificar(rubro4);
		
		listarProductos();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Editar productos(producto4)");
		producto4.setPrecio(new BigDecimal(1700));
		producto.modificar(producto4);
		listarProductos();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Eliminar productos(producto4)");
		producto.eliminar(producto4);
		listarProductos();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Activar productos(producto4)");
		producto.activar(producto4);
		listarProductos();
		System.out.println("-----------------------------------------------------------");
		System.out.println("-----------------------------------------------------------");
		System.out.println("Se agregaron los productos a los productores");
		listarProductores();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Se agregaron los productos a los rubros");
		listarRubros();
		System.out.println("-----------------------------------------------------------");
		System.out.println("-----------------------------------------------------------");
		System.out.println("\n");
		
	
		
		System.out.println("USUARIO");
		System.out.println("-----------------------------------------------------------");
		System.out.println("Agregar usuario");
		List<Pedido> pedidos1=new ArrayList<Pedido>();
		u1=new Usuario("Martin","Perez","martin_09@gmail.com","calle 8 1232",221456873,"martin1234",pedidos1,Rol.USUARIO,Estado.ACTIVO);
		List<Pedido> pedidos2=new ArrayList<Pedido>();
		u2=new Usuario("Elena","Martinez","ele213@gmail.com","calle 44 920",221342343,"123456",pedidos2,Rol.USUARIO,Estado.ACTIVO);
		u3=new Usuario("Pablo","Lopez","palo.3@gmail.com","calle 62 233",221423352,"d3REW",new ArrayList<Pedido>(),Rol.ADMINISTRADOR,Estado.ACTIVO);
		user.agregar(u1);
		user.agregar(u2);
		user.agregar(u3);
		listarUsuarios();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Editar usuario(usuario3)");
		u3.setNombre("Agustin");
		user.modificar(u3);
		listarUsuarios();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Suspender usuario(usuario3)");
		user.eliminar(u3);
		listarUsuarios();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Activar usuario(usuario3)");
		user.activar(u3);
		listarUsuarios();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Cambiar rol a usuario(usuario3)");
		user.userRol(u3);
		listarUsuarios();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Cambiar rol a administrador(usuario3)");
		user.adminRol(u3);
		listarUsuarios();
		System.out.println("-----------------------------------------------------------");
		System.out.println("-----------------------------------------------------------");
		System.out.println("\n");
		
		System.out.println("Pedido");
		System.out.println("-----------------------------------------------------------");
		System.out.println("Agregar pedido");
		List<ProductoEncargado> pEncargados=new ArrayList<ProductoEncargado>();
		pedido1=new Pedido(new Date(2023,05,22),r1.getFechaRetiro(),u1,RetiroEntrega.ENTREGA,EstadoPedido.NOCONFIRMADO,null,u1.getDireccion(),new BigDecimal(0),"9am-13am",pEncargados,r1);
		List<ProductoEncargado> pEncargados2=new ArrayList<ProductoEncargado>();
		pedido2=new Pedido(new Date(2023,05,22),r2.getFechaRetiro(),u2,RetiroEntrega.RETIRO,EstadoPedido.NOCONFIRMADO,a2,null,new BigDecimal(0),r2.getRangoHorario(),pEncargados2,r2);
		pedido.agregar(pedido1);
		pedido.agregar(pedido2);
		listarPedidos();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Editar pedido(rangoHorario pedido1)");
		pedido1.setRangoHorario("9am-16pm");
		pedido.modificar(pedido1);
		listarPedidos();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Cancelar pedido(estado)");
		pedido.eliminar(pedido1);
		listarPedidos();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Activar pedido(estado)");
		pedido.activar(pedido1);
		listarPedidos();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Confirmar pedido(estado)");
		pedido.confirmar(pedido1);
		listarPedidos();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Marcar como retirado(estado)");
		pedido.marcarComoRetirado(pedido1);
		listarPedidos();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Marcar como entregado(estado)");
		pedido.marcarComoEntregado(pedido1);
		listarPedidos();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Marcar para ser retirado(RetiroEntrega)");
		pedido.marcarParaRetiro(pedido1);
		listarPedidos();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Marcar para ser entregado(RetiroEntrega)");
		pedido.marcarParaEntrega(pedido1);
		listarPedidos();
		System.out.println("-----------------------------------------------------------");
		System.out.println("-----------------------------------------------------------");
		//AGREGO LOS PEDIDOS A LOS USUARIOS
		System.out.println("Se agregaron los pedidos a los usuarios");
		pedidos1=u1.getPedidos();
		pedidos1.add(pedido1);
		u1.setPedidos(pedidos1);
		user.modificar(u1);
		
		pedidos2=u2.getPedidos();
		pedidos2.add(pedido2);
		u2.setPedidos(pedidos2);
		user.modificar(u2);
		listarUsuarios();
		System.out.println("-----------------------------------------------------------");
		System.out.println("-----------------------------------------------------------");
		System.out.println("\n");
		
		
		System.out.println("Producto Encargado");
		System.out.println("-----------------------------------------------------------");
		System.out.println("Agregar producto encargado");
		pe1=new ProductoEncargado(2,producto1,pedido1,Estado.ACTIVO);
		pe2=new ProductoEncargado(3,producto2,pedido1,Estado.ACTIVO);
		pe3=new ProductoEncargado(1,producto4,pedido2,Estado.ACTIVO);
		pe.agregar(pe1);
		pe.agregar(pe2);
		pe.agregar(pe3);
		//modifico los precios del pedido
		pedido1.setMontoTotal(new BigDecimal(pe1.getCantidad()).multiply(producto1.getPrecio()).add(new BigDecimal(pe2.getCantidad()).multiply(producto2.getPrecio())));
		pedido2.setMontoTotal(new BigDecimal(pe3.getCantidad()).multiply(producto4.getPrecio()));
		pedido.modificar(pedido1);
		pedido.modificar(pedido2);
		//--------------------
		listarProductosEncargados();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Editar producto encargado(producto1)");
		pe1.setCantidad(1);
		pe.modificar(pe1);
		//modifico los precios del pedido
		pedido1.setMontoTotal(new BigDecimal(pe1.getCantidad()).multiply(producto1.getPrecio()).add(new BigDecimal(pe2.getCantidad()).multiply(producto2.getPrecio())));
		pedido.modificar(pedido1);
		//--------------------
		listarProductosEncargados();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Eliminar producto encargado(producto3)");
		pe.eliminar(pe3);
		listarProductosEncargados();
		System.out.println("-----------------------------------------------------------");
		System.out.println("Activar producto encargado(producto3)");
		pe.activar(pe3);
		listarProductosEncargados();
		System.out.println("-----------------------------------------------------------");
		System.out.println("-----------------------------------------------------------");
        //Agrego los productos encargados a los pedidos
		System.out.println("Se agregaron los productos encargados a los pedidos");
		pEncargados=pedido1.getListaProductos();
		pEncargados.add(pe1);
		pEncargados.add(pe2);
		pedido1.setListaProductos(pEncargados);
		pedido.modificar(pedido1);
		
		pEncargados2=pedido2.getListaProductos();
		pEncargados2.add(pe3);
		pedido2.setListaProductos(pEncargados2);
		pedido.modificar(pedido2);
		listarPedidos();
		System.out.println("-----------------------------------------------------------");
		System.out.println("-----------------------------------------------------------");
		System.out.println("\n");
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	public void listarProductosEncargados() {
		System.out.println("Lista de productos encargados:");
		List<ProductoEncargado> lista12= pe.getList();
		for(ProductoEncargado l:lista12) {
			System.out.println("-> Id: "+ l.getId()+ ", cantidad: "+ l.getCantidad()+", estado: "+ l.getEstado());
			System.out.println("    Pedido  Id:"+l.getPedido().getId()+", fecha:" + l.getPedido().getFecha()+", usuario: "+l.getPedido().getUsuario().getId()+", fecha entrega: "+l.getPedido().getFechaEntrega()+ ", retiro o entrega: "+l.getPedido().getRetiroEntrega()+ ", estado: "+ l.getPedido().getEstado()+", direccion de retiro: "+l.getPedido().getDireccionRetiro()+", direccion de entrega: "+ l.getPedido().getRetiroEntrega()+", monto total: "+ l.getPedido().getMontoTotal()+ ", rango horario: " +l.getPedido().getRangoHorario()+ "ronda: "+ l.getPedido().getRonda());
			System.out.println("    Producto: Id: "+l.getProducto().getId()+", nombre: "+l.getProducto().getNombre()+", descripcion: "+l.getProducto().getDescripcion()+", stock: "+l.getProducto().getStock()+", estado: "+l.getProducto().getEstado()+",precio: "+l.getProducto().getPrecio());
		}
	}
	public void listarPedidos() {
		System.out.println("Lista de pedidos:");
		List<Pedido> lista12= pedido.getList();
		for(Pedido l:lista12) {
			System.out.println("->Pedido  Id:"+l.getId()+", fecha:" + l.getFecha()+", fecha entrega: "+l.getFechaEntrega()+ ", retiro o entrega: "+l.getRetiroEntrega()+ ", estado: "+ l.getEstado()+", direccion de retiro: "+l.getDireccionRetiro()+", direccion de entrega: "+ l.getRetiroEntrega()+", monto total: "+ l.getMontoTotal()+ ", rango horario: " +l.getRangoHorario());
			System.out.println("      Usuario Id: "+l.getUsuario().getId()+ ", nombre: "+l.getUsuario().getNombre()+", apellido: "+l.getUsuario().getApellido()+ ", direccion: "+l.getUsuario().getDireccion()+", email: "+l.getUsuario().getEmail()+", telefono: "+l.getUsuario().getTelefono()+ ", contraseņa: "+l.getUsuario().getPassword()+ ", estado: "+l.getUsuario().getEstado()+", rol: "+l.getUsuario().getRol());
			System.out.println("      Ronda Id: "+l.getRonda().getId()+", fecha de inicio : "+ l.getRonda().getFechaInicio()+", fecha fin: "+l.getRonda().getFechaFin()+ ", fecha de retiro: "+l.getRonda().getFechaRetiro()+", rango horario: "+l.getRonda().getRangoHorario());
			for(ProductoEncargado pe:l.getListaProductos()) { 
		    	System.out.println("      ProductoEncargado id: "+ pe.getId()+ ", cantidad: "+ pe.getCantidad()+", pedido: " + pe.getPedido().getId());
		    	System.out.println("         Producto Id: "+pe.getProducto().getId()+", nombre: "+pe.getProducto().getNombre()+", descripcion: "+pe.getProducto().getDescripcion()+", stock: "+pe.getProducto().getStock()+", estado: "+pe.getProducto().getEstado()+",precio: "+pe.getProducto().getPrecio());
			}
		
		}
	}
	
	public void listarUsuarios() {
		System.out.println("Lista de usuarios:");
		List<Usuario> lista12= user.getList();
		for(Usuario l:lista12) {
			System.out.println("-> Id: "+l.getId()+ ", nombre: "+l.getNombre()+", apellido: "+l.getApellido()+ ", direccion: "+l.getDireccion()+", email: "+l.getEmail()+", telefono: "+l.getTelefono()+ ", contraseņa: "+l.getPassword()+ ", estado: "+l.getEstado()+", rol: "+l.getRol());
			for (Pedido p: l.getPedidos()) { 
				System.out.println("    Pedido  Id:"+p.getId()+", fecha:" + p.getFecha()+", fecha entrega: "+p.getFechaEntrega()+", usuario: "+ p.getUsuario().getId()+ ", retiro o entrega: "+p.getRetiroEntrega()+ ", estado: "+ p.getEstado()+", direccion de retiro: "+p.getDireccionRetiro()+", direccion de entrega: "+ p.getRetiroEntrega()+", monto total: "+ p.getMontoTotal()+ ", rango horario: " + p.getRangoHorario()+ "ronda: "+ p.getRonda());
			    for(ProductoEncargado pe:p.getListaProductos()) { 
			    	System.out.println("      ProductoEncargado id: "+ pe.getId()+ ", cantidad: "+ pe.getCantidad()+", producto: "+ pe.getProducto()+", pedido: " + pe.getPedido());
			    }
			}
		}
	}
	public void listarProductos() {
    	System.out.println("Lista de productos:");
		List<Producto> lista12= producto.getList();
		for(Producto l:lista12) {
//			System.out.print(l.toString());			
			System.out.println("-> Id: "+l.getId()+", nombre: "+l.getNombre()+", descripcion: "+l.getDescripcion()+", stock: "+l.getStock()+", estado: "+l.getEstado()+",precio: "+l.getPrecio());
		    System.out.println("   Productor:"+ " id: "+l.getProductor().getId()+", nombre: "+l.getProductor().getNombre()+ ", descripcion: "+l.getProductor().getDescripcion()+", estado: "+l.getProductor().getEstado());
		    for( RubroDeProducto r:l.getRubros()) {
		    	System.out.println("   Rubro:  id: "+ r.getId()+ ", nombre: "+ r.getNombre()+", estado: "+r.getEstado());
		    }
		}
    }
	public void listarRubros() {
    	System.out.println("Lista de rubros:");
		List<RubroDeProducto> lista= rubro.getList();
		for(RubroDeProducto l:lista) {
			System.out.println("-> Id: "+ l.getId()+", nombre: "+ l.getNombre()+ ", estado: " + l.getEstado());
			for(Producto p:l.getProductos()) {
				System.out.println("    Producto  id: "+p.getId()+ ", nombre: "+p.getNombre()+",descricion: "+p.getDescripcion()+", stock: "+ p.getStock()+ ", estado: "+ p.getEstado()+", precio: "+p.getPrecio());
			}
		}
    }
	public void listarProductores() {
    	System.out.println("Lista de productores:");
		List<Productor> lista= productor.getList();
		for(Productor l:lista) {
			System.out.println("-> Id: "+ l.getId()+ ", nombre: "+ l.getNombre()+ ", descripcion: "+ l.getDescripcion()+ ", estado: "+l.getEstado());
			for(Image m:l.getImagenes()) {
				System.out.println("    Imagen  id: "+m.getId()+ ", nombre: "+m.getNombre()+",imagen: "+m.getImagen());
			}
			for(Producto p: l.getProductos()) {
				System.out.println("    Producto  id: "+p.getId()+ ", nombre: "+p.getNombre()+",descricion: "+p.getDescripcion()+", stock: "+ p.getStock()+ ", estado: "+ p.getEstado()+", precio: "+p.getPrecio());
			}
		}
    }
    public void listarPuntoDeRetiro() {
    	System.out.println("Lista de puntos de retiro:");
		List<PuntoDeRetiro> listaPuntoDeRetiro= p.getList();
		for(PuntoDeRetiro l:listaPuntoDeRetiro) {
			System.out.println(l.toString());
		}
    }
    public void agregarPuntoDeRetiro(PuntoDeRetiro punto) {
    	p.agregar(punto);
    }
    public void eliminarPuntoDeRetiro(PuntoDeRetiro punto) {
    	System.out.println("Eliminar punto de retiro");
    	p.eliminar(punto);
    	listarPuntoDeRetiro();
    }
    public void editarPuntoDeRetiro(PuntoDeRetiro punto) {
    	System.out.println("Editar punto de retiro");
    	p.modificar(punto);
    	listarPuntoDeRetiro();
    }
    public void listarImagenes() {
    	System.out.println("Lista de imagenes:");
		List<Image> lista= m.getList();
		for(Image l:lista) {
			System.out.println(l.toString());
		}
    }
    public void listarRondas() {
    	System.out.println("Lista de rondas:");
		List<Ronda> list= r.getList();
		for(Ronda l:list) {
			System.out.println(l.toString());
		}
    }
    
}
